// Объявляем переменную numberOfFilms глобально, чтобы работать с ней внутри функции
let numberOfFilms;

// 1. Функция для проверки введенных данных при ответе на 1-ый вопрос
function start(){
    numberOfFilms = +prompt('Сколько фильмов вы уже посмотрели?');
    /* Проверка: на пустое поле для ввода ИЛИ на кнопку Отмена ИЛИ 
       на НЕ число, то снова задаем вопрос... */
    while(numberOfFilms == '' || numberOfFilms == null || isNaN(numberOfFilms)){
        numberOfFilms = +prompt('Сколько фильмов вы уже посмотрели?');
    } 
}
start(); // Вызов функции

// --------- Главный объект программы ---------
const personalMovieDB = {
    count: numberOfFilms,
    movies: {},
    actors: {},
    genres: [],
    private: false
};


function rememberMyFilms(){
    for (let i = 0; i < 2; i++) {
        const a = prompt('Один из последних просмотренных фильмов?'),
              b = prompt('На сколько оцените его?');   
        /* Проверка на пустое поле для ввода ИЛИ на кнопку Отмена ИЛИ на количество символов */
        if(a != '' && b != '' && a != null && b != null && a.length < 50) {
            personalMovieDB.movies[a] = b; 
            console.log('done');
        }
        else{
            console.log('error');
            i--; 
        }
    }
}
rememberMyFilms();

function showMyDB(){
  if(personalMovieDB.private == false){
      console.log(personalMovieDB); 
  }
}
showMyDB();
function writeYourGenres(){
  for(let i = 1; i <= 3; i++){
      const genres = prompt(`Ваш любимый жанр номер ${i}`);
      personalMovieDB.genres[i - 1] = genres;
    
  }
}
writeYourGenres();
